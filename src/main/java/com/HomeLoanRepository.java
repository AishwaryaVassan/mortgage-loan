package com;

import org.springframework.data.repository.CrudRepository;

public interface HomeLoanRepository extends CrudRepository<HomeLoan, Integer>{

}
