package com;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.Register;
@RestController
public class RegisterController {

	@Autowired
	RegisterRepository registerRepository;

	@Autowired
	HomeLoanRepository homeRepository;
	
	@Autowired
	   private ViewProfileRepository dao;

	@CrossOrigin
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView login() {
		return new ModelAndView("login");
	}

	@CrossOrigin
	@RequestMapping("/register")
	public ModelAndView register(@ModelAttribute("reg") Register register, BindingResult bindingresult) {
		if (bindingresult.hasErrors())
			return new ModelAndView("register");
		registerRepository.save(register);
		return new ModelAndView("success");
	}

	@RequestMapping(value = "/home")
	public ModelAndView home(HomeLoan homeloan) {
		homeRepository.save(homeloan);
		return new ModelAndView("raiseLoan");
	}

	@RequestMapping(value = "/home_calc",method=RequestMethod.POST)
	public ModelAndView eval_home(HomeLoan homeloan, HttpServletRequest request) {
		
		int pincode=Integer.parseInt(request.getParameter("pincode"));
		int areaVal=0;
		double propAge = Double.parseDouble(request.getParameter("propAge"));
		double propArea = Double.parseDouble(request.getParameter("propArea"));
		double propCal=0;
		double buildingVal=0;
		double FinalPropVal=0;
		double Buildcal=0;
		double buildingsize = Double.parseDouble(request.getParameter("buildingsize"));
		double loanAmt = Double.parseDouble(request.getParameter("loanAmt"));
		double currEMI=0;
		double interest=0.08;
		double loanTenure = Double.parseDouble(request.getParameter("loanTenure"));
		double oldEmi = Double.parseDouble(request.getParameter("oldEmi"));
		double salary = Double.parseDouble(request.getParameter("salary"));
		//property calculation 
		if(pincode==60021 || pincode==60022 || pincode==60023 || pincode==60024)
		{
			areaVal=2000;
		}
		else
		{
			areaVal=1000;
		}
		propCal=areaVal*propArea;
		
		
	//Building Calculation
		if (propAge <= 10 && propAge>=1) {
			buildingVal = 10;
		} else if ( propAge >= 11 && propAge <=20) {
			buildingVal = 5;
		}

		else {
			System.out.println("Building too old to give Mortgage loan");
			FinalPropVal = 0;
		}

		Buildcal = buildingsize*buildingVal;
		System.out.println(Buildcal);
		//console.log("buildingCal : " + this.Buildcal);
		
		FinalPropVal=propCal+Buildcal;
		
		
		
	currEMI=(((loanAmt)*(interest)*(1+interest)*loanTenure)/((1+interest)*((loanTenure)-1))); 
		
	double TotalEMI=currEMI+oldEmi;
	
	if(TotalEMI< (60*salary)/100)
	{
		if(FinalPropVal>loanAmt)
		{
			System.out.println("Approval");
			return new ModelAndView("success");
		}
	}
		
		
		
		
	return new ModelAndView("raiseloan");
		
		
	}

	List<Login> list = new ArrayList();

	@RequestMapping("/login")
	public ModelAndView loginu(HttpServletRequest request, @ModelAttribute("login") Login login, HttpSession session)
			throws Exception {
		String name = request.getParameter("username");
		String pswd = request.getParameter("password");
		String name1 = "", pswd1 = "";
		if (name.equalsIgnoreCase("admin") && pswd.equalsIgnoreCase("admin")) {
			return new ModelAndView("success");
		} else {
			for (Login l : list) {
				name1 = l.getUsername();
				pswd1 = l.getPassword();
				if (name1.equals(name) && pswd1.equals(pswd)) {
					return new ModelAndView("user");
				}

			}
		}
		return new ModelAndView("login");
	}
	
	@CrossOrigin
	@RequestMapping(value = "/view") 
	    public ModelAndView viewAll() throws Exception{
	       
	List<Register> list = (List<Register>) dao.findAll();
	          return new ModelAndView("success", "list", list);

	    }
	
	
	//Testing 
	@CrossOrigin
	@RequestMapping(value = "/testregister", method = RequestMethod.GET)
	public Register firstPage() {

	Register regtest = new Register();
	regtest.setName("aishu");
	regtest.setDob("2018-12-02");
	regtest.setPhone("9626908000");;
	regtest.setAddress("chennai");
	regtest.setAadhar(1234);;
	regtest.setPAN("12345");
	regtest.setPassword("1234");

	return regtest;
	}
	
	
	@RequestMapping(value = "/testlogin", method = RequestMethod.GET)
	public Register secondPage() {
		Register regtest2 = new Register();
		regtest2.setName("aishu");
		regtest2.setPassword("1234");
	return regtest2;
	}

	@RequestMapping(value = "/testloan", method = RequestMethod.GET)
	public HomeLoan thirdPage() {
		HomeLoan loan = new HomeLoan();
		loan.setBuildingsize(23.4);
		loan.setPincode(60021);
		loan.setPropArea(10);
		loan.setPropAge(1);
	return loan;
	}

	

	}

